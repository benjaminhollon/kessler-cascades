---

title: Solutions

---

On this site, I discuss [the theory behind Kessler Cascades](/) in great detail. Knowing that a problem exists, though, is not enough. We need practical solutions to combat and reduce the onset of Kessler Syndrome.

Here are some proposals that are actively being worked on by governments, organizations, and individuals around the world.

## Capture and Return {id="removal"}

No matter what else we do, we're eventually going to bring some of the junk in orbit back down to Earth. A stable situation in orbit means Debris is being removed at the same rate it is generated.

Now, debris does naturally deorbit. Trace amounts of Earth's atmosphere remain, even at the insane altitudes of orbital satellites, and these slow satellites down over the course of years to decades, eventually causing them to fall back to Earth. This effect does give us a base number of satellites we can launch each year without sparking a Cascade.

In the last few years, the space industry has been massively ramping up the number of satellites it's launching, and this likely won't stop anytime soon. To be able to maintain this rapid progress without sacrificing [services that we love](/#impact), we need to supplement the natural deorbiting of debris with some of our own creative solutions.

## Large Debris Removal {id="removal__large"}

The largest pieces of debris (>10cm in diameter) are the highest priority to remove, since they are the easiest for satellites to collide with (due to their size) and have the potential to create more pieces of debris upon collision.

Luckily, they also have the most straightforward plans for removal.

While dozens of far-fetched proposals exist, all the most reasonable solutions have the same basic idea behind them: launch a probe that meets up with a large piece of debris, connects with it, and tugs it back down into the atmosphere.

[The European Space Agency is currently planning a mission to implement this strategy.](https://sea.pcmag.com/news/40569/the-european-space-agency-is-going-to-use-a-giant-claw-to-grab-space-junk)

## Medium Debris Removal {id="removal__medium"}

For pieces of debris between 1cm and 10cm in diameter, the most practical solution also boasts the coolest name: the [Laser Broom](https://en.wikipedia.org/wiki/Laser_broom).

<figure>

![Artistic representation of a Laser Broom in action.](/assets/images/Laser_broom_%28artistic%29.jpg)

<figcaption>Artistic representation of a Laser Broom in action. <a href="https://commons.wikimedia.org/wiki/File:Laser_broom_(artistic).jpg">(Source, CC0)</a></figcaption>

</figure>

A laser broom would use a ground-based laser facility near Earth's equator to heat debris enough to change their orbits to ones that will deorbit faster. [A single laser broom facility near Earth's equator could deorbit all 150,000+ medium-sized debris fragments under 1,500km in altitude within only 3-5 years.](https://ntrs.nasa.gov/citations/20100002197)

## Small Debris Removal {id="removal__small"}

Sadly, [there is not yet a practical solution for removing debris smaller than 1cm in diameter](https://ntrs.nasa.gov/citations/20100002197). In addition, pieces this small are nearly impossible to track, though our best estimates project millions of these fragments.

Debris as small as 0.2mm in diameter are safety hazards for satellites, and debris larger than 5mm can be mission-ending. Even a screw has the kinetic energy of a cannonball at orbital velocities, [able to shred specially-woven kevlar fabric](https://www.esa.int/Safety_Security/Space_Debris/Hypervelocity_impacts_and_protecting_spacecraft).

<figure>

![The result of hypervelocity impact testing by the European Space Agency, a crater in a huge block of metal..](/assets/images/Hypervelocity_Impact.png)

<figcaption>The result of hypervelocity impact testing by the European Space Agency. <a href="https://www.esa.int/Safety_Security/Space_Debris/Hypervelocity_impacts_and_protecting_spacecraft">(Source, CC BY-SA 3.0 IGO)</a></figcaption>

</figure>

## Tracking {id="tracking"}

What if we could better predict where debris and satellites will be at any given moment? That could help us avoid collisions more often, buying time for researchers to figure out Capture and Return technology.

I'd like to introduce you to [ELROI, the Extremely Low-Resource Optical Identifier.](https://digitalcommons.usu.edu/cgi/viewcontent.cgi?article=4131&context=smallsat)

Billed as "[a] license plate for satellites that anyone can read," ELROI provides an innovative approach that aims to remove the workload of identifying from critical telescopes that are needed to identify debris (which is unable to identify itself). It works by firing a unique series of laser pulses at the Earth that could be conceivably be detected even by an amateur astronomer with the right equipment.

What I love about this approach is how well it scales: it could enable volunteer organizations to handle the identification and tracking of telescopes with minimal effort and to easily find contact information for satellite operators if a collision is likely. The telescopes that have been used for this task can switch their efforts to instead locating debris that endangers satellites using ELROI and warn the operators in the same way.

Of course, this approach loses a massive amount of effectiveness if it only has limited use. We'll likely need some form of legislation to help encourage its adoption.

## Legislation {id="legislation"}

We're running out of time fast. If governments don't begin taking action to implement the plans above <em>now</em>, they might not have a chance later.

To buy time, we should consider legislation to help slow down debris formation.

### ELROI Legislation

As you may have surmised, [ELROI](#tracking) would be largely useless without legislation in place to encourage it.

First, any ELROI benefits will likely be minimal if only a few satellites use it. Legislation to require and potentially subsidize ELROI use would be ideal, though there are less extreme alternatives.

Additionally, an internationally-recognized body to facilitate volunteer and professional efforts to track satellites and notify operators of impending collisions would go a great way to ease the load on critical infrastructure and avoid competing standards.

<figure>

![How Standards Proliferate
(See: A/C chargers, character encodings, instant messaging, etc.)
Situation:
There are 14 competing standards.
Cueball: 14?! Ridiculous! We need to develop one universal standard that covers everyone's use cases.
Ponytail: Yeah!
Soon:
Situation: There are 15 competing standards.](https://imgs.xkcd.com/comics/standards.png) {title="Fortunately, the charging one has been solved now that we've all standardized on mini-USB. Or is it micro-USB? Shit."}

<figcaption>by Randall Munroe for <a href="https://xkcd.com" target="_blank" rel="noopener">xkcd</a>. <a href="https://xkcd.com/927/">(Source, CC BY-NC 2.5)</a></figcaption>

</figure>

### Satellite Destruction Test Ban

One of the more awful contributors to this whole mess is Satellite Destruction Tests. The United States, Russia, China, and India have all launched missiles at their own derelict satellites in anti-satellite tests. These tests, while useful for research purposes, have created thousands of pieces of debris that endanger other nations' satellites.

<a href="https://treaties.unoda.org/t/outer_space" target="_blank" rel="noopener">Current international legislation<a> does not cover these tests clearly enough, and <a href="https://www.sipri.org/sites/default/files/2021-04/eunpdc_no_74.pdf" target="_blank" rel="noopener">new proposals</a> should be seriously considered by lawmakers to ensure that space remains a safe environment for industry and international cooperation.

## Conclusion {id="conclusion"}

While there are myriad amazing technologies under development by some of the most brilliant minds alive today, <a href="/" target="_blank" rel="noopener">no real solution can be successful without international collaboration and legislation</a>.

<a href="/act/">Find out how you can help here.</a>
