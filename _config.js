import lume from "lume/mod.ts";

const markdown = {
	options: {
		breaks: true,
		typographer: true
	}
};

const site = lume({}, { markdown });

site.copy("assets");
site.copy(".htaccess");
site.copy(".stfolder");

export default site;
