---

title: Kessler Cascades

---

As we begin to think consciously about pollution and cleaning up Earth, a solution many have thought of as ideal is to move critical infrastructure such as [internet services](https://www.starlink.com/) into space.

The reality is far more complex.

In theory, space would allow us to maintain our standard of living without polluting the Earth we live in. Since satellites stay in orbit once launched (spoiler alert: they don't), maintenance costs are lower as well, right?

While <a href="https://sciencing.com/negative-effects-pollution-5268664.html" target="_blank" rel="noopener">pollution on Earth</a> leads to climate change, health issues, and decreased crop growth, pollution in orbit has the potential to bar space to humanity for centuries.

## What is Kessler Syndrome? {id="intro"}

**Kessler Syndrome**, [postulated by Donald J. Kessler and Burton G. Cour-Palais in 1978](https://ia902902.us.archive.org/27/items/kesslercollisionfrequency1978/Kessler_Collision_Frequency_1978.pdf), describes a scenario where the debris from a satellite collision hits other satellites, in turn creating more debris and sparking a collision chain which ends when every satellite in orbit is demolished.

A chain of collisions such as this is known as a **strong Kessler Cascade**.

While no cascades have occured yet, we are beginning to see the signs of a potential future cascade.

## Why do I care? I'm not a space nerd like you seem to be. {id="impact"}

That's a fair point, and many people have asked that.

The truth is, space is deeply integrated into our modern technology to a level most people don't realize. How often have you used a GPS instead of a paper map? Do you rely on Mobile Data to stay connected while on the go? Do you check the weather report before taking a walk?

All of these services (and more) are provided by satellites. The Global Positioning System (GPS) relies on satellites in geosynchronous orbit, many internet and mobile data companies use satellites ([including in Malaysia](https://www.ses.com/press-release/east-and-west-malaysia-gain-enhanced-mobile-broadband-ses-networks-and-compudyne), where I live), satellites help gather climate and weather data, and many emergency response systems rely on satellite technology to function.

For more information about how satellites affect our daily lives, from paying with bank cards to safely crossing bridges to eating cereal, check out <a href="https://www.asc-csa.gc.ca/eng/satellites/everyday-lives/10-ways-that-satellites-helped.asp" target="_blank" rel="noopener">this article from the Canadian Space Agency on ten ways satellite technology has helped you <em>today</em></a>.

Are you really willing to go without all that? Because post-cascade, you'd have to.

## How long does a Kessler Cascade take? {id="time"}

While the 2013 film [Gravity](https://www.imdb.com/title/tt1454468/) portrays a Kessler cascade unfolding over a matter of hours, a real cascade would likely take place over a time on the order of years or decades.

<figure>

<iframe style="aspect-ratio: 2 / 1; width: 100%" src="https://www.youtube.com/embed/ufsrgE0BYf0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</figure>

This has positive and negative implications.

On the plus side, we should be able to take action even during an actual cascade. On the other hand, we might not be able to tell that a cascade is happening for years, until it's too far to stop.

## Is this actually urgent? {id="urgent"}

In 2007, NASA's chief orbital debris scientist projected that removing 5-20 large pieces of debris per year beginning in 2020 could stabilize the debris situation for centuries without sparking a cascade.

<figure>

![A graph from a presentation by Nicholas L. Johnson at the 2007 Space Situational Awareness Conference demonstrating the above statement.](/assets/images/adr-projections.png)

<figcaption>From a presentation by Nicholas L. Johnson at the 2007 Space Situational Awareness Conference</figcaption>

</figure>

Welcome to 2023 (or later, if you're from the future)! We've passed the start date for this plan, and nothing's been done. What's more, companies like SpaceX have been rapidly increasing the number of satellites in Low Earth Orbit at an unprecedented rate.

SpaceX's new <a href="https://www.starlink.com/" target="_blank" rel="noopener">Starlink</a> is a prime example: they've launched thousands of satellites so far, out of a planned 12,000 satellite first-generation constellation, and SpaceX satellites are already <a href="https://www.space.com/spacex-starlink-satellite-collision-alerts-on-the-rise" target="_blank" rel="noopener">estimated to be involved in 50% of all close passes (within 1km) between satellites.</a> They're not alone; Amazon has recently announced plans to launch their own <a href="https://www.aboutamazon.com/news/tag/project-kuiper?tag=wwwfccom-20" target="_blank" rel="noopener">"Project Kuiper"</a>, which is planned to have 3,236 satellites, also in Low Earth Orbit.

This trend is not likely to stop. The overwhelming success of space industries in the last few years coupled with the rapidly falling price of satellite launches will motivate even more companies to dream big and move their infrastructure to space, furthering the risk of a Kessler Cascade.

## What's next? {id="act"}

First, take a look at some <a href="/solutions/">proposed solutions</a> to the problem to see where we currently stand in our efforts to prevent a cascade. Spoiler alert: we're going to need international collaboration and legisltaion to make it happen.

To see how <em>you</em> can help, we've put together <a href="/act/">a list of ways to take action now</a>.

<a href="/pledge/">Take a moment to look at how many people have pledged their support toward preventing a future Kessler Cascade.</a>
