---

title: Act

---

## Add Your Name

To prevent a Kessler cascade, we need international collaboration and legislation. To show your support, please consider adding your name! This will help others see how many people care about this issue, hopefully motivating lawmakers to take action.

[Read and sign the pledge here.](/pledge/)

## Raise Awareness

To make change, we need as many people as possible to know about this grave issue! The easiest way is simply to share the link <a href="/" target="_blank" rel="noopener">https://kesslercascades.com</a> with others.

If you'd like to help contribute to this website's code, you can at the <a href="https://github.com/benjaminbhollon/kessler-cascades" target="_blank" rel="noopener">Github Repository</a>.

---

This website's content is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="noopener">CC BY-SA 4.0</a>. You are free to share and adapt it in any format and for any purpose as long as you give appropriate credit and also distribute it under the same license, stating what changes you made (if any).

This website's code is licensed under the <a href="https://codeberg.org/benjaminhollon/kessler-cascades/blob/main/LICENSE">GNU GPL-3.0</a>. You are free to share and adapt it in any format and for any purpose as long as your provide source code, use the same license, include the license and copyright notice, and state what changes you've made (if any).
