---

title: Pledge Your Support

---

We, the undersigned, having an understanding of the risks of catastrophic debris collision chains, commonly known as <strong>Kessler cascades</strong>, agree that the global community should be taking all necessary and reasonable steps possible to safeguard space for long-term exploration, industrial use, and habitability.

We also urge our respective governments to take every step possible to promote international collaboration, enact legislation, and propose plans to prevent future Kessler cascades to the fullest extent they are able.

Inconsiderate practices and policies with regard to the usage of space such as <a href="https://spacenews.com/why-we-need-to-avoid-more-anti-satellite-tests/" target="_blank" rel="noopener">anti-satellite tests</a> and <a href="https://www.forbes.com/sites/williampentland/2011/05/07/congress-bans-scientific-collaboration-with-china-cites-high-espionage-risks/?sh=462cd8cc4562" target="_blank" rel="noopener">the prohibition of direct bilateral NASA-CNSA collaboration</a> need to be banned at the national and/or international levels as appropriate for the good of society at large.

<a href="https://treaties.unoda.org/t/outer_space">Even under the limited existing international legislation</a>, it is clear that "[t]he exploration and use of outer space … shall be carried out for the benefit and in the interests of all countries … and shall be the province of all mankind." In this vein, the responsibility for preventing Kessler cascades belongs to all able nations, and the problem needs to be adressed in a manner fit to that same international treaty's stated goal of "promoting international co-operation and understanding."

We, in the limited manner possible for unelected citizens, commit to promote these ideals in our communities to the best of our abilities and to take every available opportunity help the situation.

### Sign Your Name

_This form has closed, but we still encourage you to adhere to this pledge._

## Signatures {id="signatures"}

**JD Thompson** from USA
**Zachary Montgomery** from USA
**Daniel W** from Canada
**Ray Lee** from Malaysia
**Aaron Smith** from U.S. (WA State)
**Tee Yu** from Singapore
**AB Curiel** from United States
**Jonathan H** from USA
**Steve Smith** from USA
**Shehma Faizal** from India
**Antonio Cheong** from Malaysia
**Peter Hollon** from USA
**Tian Yi Zhang** from from China Mainland
